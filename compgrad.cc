/* ---------------------------------------------------------------------
 *
 * Copyright (C) 2000 - 2021 by the deal.II authors
 *
 * This file is part of the deal.II library.
 *
 * The deal.II library is free software; you can use it, redistribute
 * it, and/or modify it under the terms of the GNU Lesser General
 * Public License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 * The full text of the license can be found in the file LICENSE.md at
 * the top level directory of deal.II.
 *
 * ---------------------------------------------------------------------

 *
 * Author: Wolfgang Bangerth and Ralf Hartmann, University of Heidelberg, 2000
 */

#include <deque>

#include <boost/program_options.hpp>

using namespace boost::program_options;

#include <deal.II/base/quadrature_lib.h>
#include <deal.II/base/function.h>
#include <deal.II/base/logstream.h>
#include <deal.II/lac/vector.h>
#include <deal.II/lac/full_matrix.h>
#include <deal.II/lac/sparse_matrix.h>
#include <deal.II/lac/sparse_direct.h>
#include <deal.II/lac/dynamic_sparsity_pattern.h>
#include <deal.II/lac/solver_cg.h>
#include <deal.II/lac/solver_gmres.h>
#include <deal.II/lac/precondition.h>
#include <deal.II/lac/affine_constraints.h>
#include <deal.II/grid/tria.h>
#include <deal.II/grid/grid_generator.h>
#include <deal.II/grid/grid_refinement.h>
#include <deal.II/grid/grid_out.h>
#include <deal.II/dofs/dof_handler.h>
#include <deal.II/dofs/dof_tools.h>
#include <deal.II/fe/fe_q.h>
#include <deal.II/numerics/matrix_tools.h>
#include <deal.II/numerics/error_estimator.h>
#include <deal.II/numerics/data_out.h>

#include <deal.II/dofs/dof_renumbering.h>
#include <deal.II/base/smartpointer.h>
#include <deal.II/numerics/vector_tools.h>
#include <deal.II/base/convergence_table.h>
#include <deal.II/fe/fe_values.h>

#include <array>
#include <vector>
#include <fstream>
#include <iostream>

using namespace dealii;

// number of cells per axis in coarse grid
uint    ncoarse           = 0;

// use weighted matchings
bool    weighted_matching = false;

enum gradient_method_t {
    at_cell_center,
    cell_average,
    at_matching_points,
    at_quadrature_points
};

//////////////////////////////////////////////////////////
//
// local functions
//
//////////////////////////////////////////////////////////

// return square of x
template < typename T >  T     square ( T x )      { return x*x; }

// return true of x is "near" y
template < typename T >  bool  near   ( T x, T y, double eps = 1e-2 ) { return std::abs( x - y ) < T(eps); }

// "near" for points
template < int dim >
bool
near   ( const Point< dim > &  x,
         const Point< dim > &  y,
         const double          dist = 1e-8 )
{
    return x.distance( y ) < dist;
}

void
write_vecfield_vtk ( const std::string &                    filename,
                     const std::string &                    varname,
                     const std::vector< Point< 2 > > &      origins,
                     const std::vector< Tensor< 1, 2 > > &  directions )
{
    const auto     npts = origins.size();
    std::ofstream  out( filename.c_str() );

    out << "# vtk DataFile Version 3.0" << std::endl
        << "# generated" << std::endl
        << "ASCII" << std::endl
        << "DATASET UNSTRUCTURED_GRID" << std::endl
        << "" << std::endl
        << "POINTS " << npts << " double" << std::endl;

    for ( uint i = 0; i < npts; ++i )
        out << origins[i](0) << " " << origins[i](1) << " 0" << std::endl;

    out << std::endl
        << "POINT_DATA " << npts << std::endl
        << "VECTORS " << varname << " double" << std::endl;
        
    for ( uint i = 0; i < npts; ++i )
        out << directions[i][0] << " " << directions[i][1] << " 0" << std::endl;
}

void
write_vecfield_py ( const std::string &                    filename,
                    const std::string &                    varname,
                    const std::vector< Point< 2 > > &      origins,
                    const std::vector< Tensor< 1, 2 > > &  directions )
{
    std::ofstream  out( filename.c_str() );

    out << varname << " = [" << std::endl;

    for ( uint i = 0; i < origins.size(); ++i )
        out << "  [ "
            << origins[i](0) << ", " << origins[i](1) << ", "
            << directions[i][0] << ", " << directions[i][1]
            << " ]," << std::endl;

    out << "]" << std::endl;
}

double  domain_lb = 0;
double  domain_ub = 0;

struct matching_entry
{
    double  x0, x1;
    double  y0, y1;
    double  weight;
};

struct normal_entry
{
    double  cx, cy;
    double  u, v;
    double  sign;
};

std::vector< matching_entry >  matching_data;
std::vector< normal_entry >    normal_data;

void
read_matching_data ( const std::string &  filename )
{
    std::ifstream  in( filename );

    std::deque< matching_entry >  data;

    while ( in )
    {
        double  x0, x1, y0, y1, w;

        in >> x0 >> x1 >> y0 >> y1 >> w;
        data.push_back( { x0, x1, y0, y1, w } );
    }// while

    matching_data.resize( data.size() );

    std::copy( data.begin(), data.end(), matching_data.begin() );
}

void
read_normal_data ( const std::string &  filename )
{
    std::ifstream  in( filename );

    in >> domain_lb;
    in >> domain_ub;

    std::deque< normal_entry >  data;

    while ( in )
    {
        double  cx, cy, u, v, sign;

        in >> cx >> cy >> u >> v >> sign;
        data.push_back( { cx, cy, u, v, sign } );
    }// while

    normal_data.resize( data.size() );

    std::copy( data.begin(), data.end(), normal_data.begin() );
}

class Problem
{
private:
    Triangulation<2> triangulation;
    DoFHandler<2>    dof_handler;

    SmartPointer<const FiniteElement<2>> fe;

    AffineConstraints<double> hanging_node_constraints;

    SparsityPattern      sparsity_pattern;
    SparseMatrix<double> system_matrix;
    Vector<double>       solution;
    Vector<double>       system_rhs;
    
public:
    Problem( const FiniteElement<2> & fe );

    void run( uint  nlvl );

private:
    void setup_system();
    void assemble_system();
    void solve();

    void compute_gradient ( const Vector< double > &         phi,
                            std::vector< Tensor< 1, 2 > > &  gradient,
                            std::vector< Point< 2 > > &      dof_points,
                            const gradient_method_t          method ) const;
    
};

Problem::Problem( const FiniteElement<2> &fe )
        : dof_handler(triangulation)
        , fe(&fe)
{}

void Problem::setup_system()
{
    dof_handler.distribute_dofs(*fe);
    
    std::cout << "     no. of degrees of freedom: "
              << dof_handler.n_dofs()
              << std::endl;

    DoFRenumbering::Cuthill_McKee(dof_handler);

    hanging_node_constraints.clear();
    DoFTools::make_hanging_node_constraints(dof_handler, hanging_node_constraints);
    hanging_node_constraints.close();

    DynamicSparsityPattern dsp(dof_handler.n_dofs(), dof_handler.n_dofs());
    
    DoFTools::make_sparsity_pattern(dof_handler, dsp);
    hanging_node_constraints.condense(dsp);
    sparsity_pattern.copy_from(dsp);

    system_matrix.reinit(sparsity_pattern);

    solution.reinit(dof_handler.n_dofs());
    system_rhs.reinit(dof_handler.n_dofs());
}

void Problem::assemble_system()
{
    QGauss<2>  quadrature_formula(fe->degree + 1);
    QGauss<1>  face_quadrature_formula(fe->degree + 1);

    const auto  n_q_points      = quadrature_formula.size();

    const auto  dofs_per_cell   = fe->n_dofs_per_cell();

    FullMatrix<double> cell_matrix( dofs_per_cell, dofs_per_cell );
    Vector<double>     cell_rhs( dofs_per_cell );

    std::vector<types::global_dof_index> local_dof_indices( dofs_per_cell );

    FEValues<2>     fe_values( *fe,
                               quadrature_formula,
                               update_values | update_gradients |
                               update_quadrature_points | update_JxW_values);
    
    FEFaceValues<2> fe_face_values( *fe,
                                    face_quadrature_formula,
                                    update_values | update_quadrature_points |
                                    update_normal_vectors |
                                    update_JxW_values );

    std::vector<double>   rhs_values(n_q_points);

    auto  vtx_mapping = StaticMappingQ1< 2 >::mapping;
    auto  const_fn    = Vector<double>( dof_handler.n_dofs() );

    for ( uint  i = 0; i < dof_handler.n_dofs(); ++i )
        const_fn[i] = 1.0;
    
    for (const auto &  cell : dof_handler.active_cell_iterators())
    {
        cell_matrix = 0.;
        cell_rhs    = 0.;

        fe_values.reinit( cell );

        for (unsigned int i = 0; i < dofs_per_cell; ++i)
        {
            for (unsigned int j = 0; j < dofs_per_cell; ++j)
            {
                for (unsigned int q_point = 0; q_point < n_q_points; ++q_point)
                {
                    cell_matrix(i, j) += (fe_values.shape_grad(i, q_point) *   // grad phi_i(x_q)
                                          fe_values.shape_grad(j, q_point) *   // grad phi_j(x_q)
                                          fe_values.JxW(q_point));             // dx
                }// for
            }// for
        }// for

        for ( auto  match : matching_data )
        {
            {
                const auto  X      = Point< 2 >{ match.x0, match.x1 };
                const auto  X_unit = vtx_mapping.transform_real_to_unit_cell( cell, X );

                // if ( GeometryInfo<2>::is_inside_unit_cell( X_unit, tolerance ) )
                if ( cell->point_inside( X ) )
                {
                    const Quadrature<2>  quadrature( GeometryInfo<2>::project_to_unit_cell( X_unit ) );
                    FEValues<2>          pt_fe_values( *fe, quadrature, update_values );
                        
                    pt_fe_values.reinit( cell );

                    for ( unsigned int i = 0; i < dofs_per_cell; ++i )
                        cell_rhs(i) -= pt_fe_values.shape_value( i, 0 );
                }// if
            }

            {
                const auto  Y      = Point< 2 >{ match.y0, match.y1 };
                const auto  Y_unit = vtx_mapping.transform_real_to_unit_cell( cell, Y );

                // if ( GeometryInfo<2>::is_inside_unit_cell( Y_unit, tolerance ) )
                if ( cell->point_inside( Y ) )
                {
                    const Quadrature<2>  quadrature( GeometryInfo<2>::project_to_unit_cell( Y_unit ) );
                    FEValues<2>          pt_fe_values( *fe, quadrature, update_values );
                        
                    pt_fe_values.reinit( cell );

                    for ( unsigned int i = 0; i < dofs_per_cell; ++i )
                        cell_rhs(i) += pt_fe_values.shape_value( i, 0 );
                }// if
            }
        }// for

        for ( auto entry : normal_data )
        {
            const auto  CX      = Point< 2 >{ entry.cx, entry.cy };
            const auto  CX_unit = vtx_mapping.transform_real_to_unit_cell( cell, CX );
            
            if ( cell->point_inside( CX ) )
            {
                for ( const auto &  face : cell->face_iterators() )
                {
                    if ( face->at_boundary() ) // need also to check if point is _on face_
                    {
                        const Quadrature<2>  quadrature( GeometryInfo<2>::project_to_unit_cell( CX_unit ) );
                        FEValues<2>          pt_fe_values( *fe, quadrature, update_values );
                        
                        pt_fe_values.reinit( cell );

                        for (unsigned int i = 0; i < dofs_per_cell; ++i)
                            cell_rhs(i) += entry.sign * pt_fe_values.shape_value( i, 0 );

                        // avoid double evaluation
                        break;
                    }// if
                }// for
            }// if
        }// for
        
        cell->get_dof_indices( local_dof_indices );
        
        for ( unsigned int i = 0; i < dofs_per_cell; ++i )
        {
            for (unsigned int j = 0; j < dofs_per_cell; ++j)
                system_matrix.add( local_dof_indices[i],
                                   local_dof_indices[j],
                                   cell_matrix(i, j) );
            
            system_rhs( local_dof_indices[i]) += cell_rhs(i);
        }// for
    }

    hanging_node_constraints.condense( system_matrix );
    hanging_node_constraints.condense( system_rhs );
}


void Problem::solve()
{
    if ( true )
    {
        std::cout << "━━ solving using direct solver" << std::endl;

        SparseDirectUMFPACK  solver;

        solver.initialize( system_matrix );

        solution = system_rhs;
        solver.solve( solution );
    }// if
    else
    {
        SolverControl            solver_control(100000, 1e-8);
        SolverCG<Vector<double>> cg( solver_control );
      
        PreconditionSSOR<SparseMatrix<double>> preconditioner;
      
        preconditioner.initialize( system_matrix, 0.5 );
      
        cg.solve(system_matrix, solution, system_rhs, preconditioner);

        std::cout << "     " << solver_control.last_step()
                  << " iterations needed to obtain convergence."
              << std::endl;
    }// else
    
    hanging_node_constraints.distribute( solution );
}


void Problem::run( uint  nlvl )
{
    auto  square = [] ( const auto  f ) { return f*f; };
    
    GridGenerator::hyper_cube( triangulation, domain_lb, domain_ub );

    triangulation.refine_global( nlvl );
              
    setup_system();

    assemble_system();
    solve();

    std::string vtk_filename;

    vtk_filename = "solution.vtk";
      
    std::ofstream output( "solution.vtk" );

    DataOut<2> data_out;
    data_out.attach_dof_handler(dof_handler);
    data_out.add_data_vector( solution, "solution" );

    data_out.build_patches(fe->degree);
    data_out.write_vtk(output);

    std::vector< Point< 2 > >     grad_origin;
    std::vector< Tensor< 1, 2 > > grad_dir;
        
    {
        compute_gradient( solution, grad_dir, grad_origin, at_cell_center );

        write_vecfield_vtk( "gradient.vtk", "gradient", grad_origin, grad_dir );
        write_vecfield_py( "gradient.py", "gradient", grad_origin, grad_dir );
    }

    {
        std::vector< Point< 2 > >     cross( normal_data.size() );
        std::vector< Tensor< 1, 2 > > normals( normal_data.size() );

        for ( uint  i = 0; i < normal_data.size(); ++i )
        {
            cross[i](0)   = normal_data[i].cx;
            cross[i](1)   = normal_data[i].cy;
            normals[i][0] = normal_data[i].u;
            normals[i][1] = normal_data[i].v;
            
            if ( near( normal_data[i].cx, domain_lb, 1e-4 ) )
            {
                if ( normal_data[i].u < 0 ) cross[i](0) -= normal_data[i].u;
            }// if
            else if ( near( normal_data[i].cx, domain_ub, 1e-4 ) )
            {
                if ( normal_data[i].u > 0 ) cross[i](0) -= normal_data[i].u;
            }// else
            else if ( near( normal_data[i].cy, domain_lb, 1e-4 ) )
            {
                if ( normal_data[i].v < 0 ) cross[i](1) -= normal_data[i].v;
            }// if
            else if ( near( normal_data[i].cy, domain_ub, 1e-4 ) )
            {
                if ( normal_data[i].v > 0 ) cross[i](1) -= normal_data[i].v;
            }// else

        }// for

        write_vecfield_vtk( "normals.vtk", "normals", cross, normals );
    }

    {
        GridOut        grid_out;
        std::ofstream  out( "grid.vtk" );
        
        grid_out.write_vtk( triangulation, out );
    }

    double  avg_match_len = 0;
    
    {
        std::vector< Point< 2 > >     X( matching_data.size() );
        std::vector< Tensor< 1, 2 > > Y( matching_data.size() );

        for ( uint  i = 0; i < matching_data.size(); ++i )
        {
            X[i](0) = matching_data[i].x0;
            X[i](1) = matching_data[i].x1;
            Y[i][0] = matching_data[i].y0 - X[i](0);
            Y[i][1] = matching_data[i].y1 - X[i](1);

            avg_match_len += std::sqrt( square( Y[i][0] ) + square( Y[i][1] ) );
        }// for

        avg_match_len /= double( matching_data.size() );

        write_vecfield_vtk( "matchings.vtk", "matchings", X, Y );
    }

    auto    cell_len = domain_ub - domain_lb;
    size_t  ncells   = 0;
    
    {
        //
        // cell size (width/height)
        //

        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            const Quadrature<2>  quadrature( { Point< 2 >{ 0, 0 }, Point< 2 >{ 1, 0 } } );
            FEValues<2>          fe_values( *fe, quadrature, update_quadrature_points );
                
            fe_values.reinit( cell );
                    
            const auto  len = ( fe_values.quadrature_point( 0 ) - fe_values.quadrature_point( 1 ) ).norm();

            if ( len < cell_len )
                cell_len = len;

            ncells++;
        }// for

        std::cout << "     no. of cells: " << ncells << std::endl;
        std::cout << "     cell length : " << cell_len << std::endl;
        
        //
        // average matchings within cell
        //
        
        std::deque< Point< 2 > >     pts;
        std::deque< Tensor< 1, 2 > > dirs;
        size_t                       min_per_cell = matching_data.size();
        size_t                       max_per_cell = 0;
        size_t                       tot_match    = 0;
        
        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            //
            // determine center of cell
            //
            
            const Quadrature<2>  quadrature( Point< 2 >{ 0.5, 0.5 } );
            FEValues<2>          fe_values( *fe, quadrature, update_quadrature_points );
                
            fe_values.reinit( cell );
                    
            const auto  cell_center = fe_values.quadrature_point( 0 );

            //
            // average all matchings originating/ending in cell
            //

            Tensor< 1, 2 >  S;
            uint            nmatch = 0;

            S[0] = S[1] = 0.0;
            
            for ( size_t  nm = 0; nm < matching_data.size(); ++nm )
            {
                const auto  X = Point< 2 >{ matching_data[nm].x0, matching_data[nm].x1 };
                const auto  Y = Point< 2 >{ matching_data[nm].y0, matching_data[nm].y1 };
                const auto  D = Y - X;

                if ( cell->point_inside( X ) )
                {
                    const auto  dist = X - cell_center;
                    const auto  s    = ( weighted_matching ? std::exp( - square( 2.0 / cell_len * dist.norm() ) ) : 1.0 );
                    
                    S[0] += s * D[0];
                    S[1] += s * D[1];
                    nmatch++;
                }// if

                if ( cell->point_inside( Y ) )
                {
                    const auto  dist = Y - cell_center;
                    const auto  s    = ( weighted_matching ? std::exp( - square( 2.0 / cell_len * dist.norm() ) ) : 1.0 );
                    
                    S[0] += s * D[0];
                    S[1] += s * D[1];
                    nmatch++;
                }// if
            }// for

            if ( nmatch > 0 )
            {
                S[0] /= double(nmatch);
                S[1] /= double(nmatch);
            }// if

            pts.push_back( cell_center );
            dirs.push_back( S );

            min_per_cell = std::min< size_t >( min_per_cell, nmatch );
            max_per_cell = std::max< size_t >( max_per_cell, nmatch );
            tot_match   += nmatch;
        }// for

        std::vector< Point< 2 > >     vpts( pts.size() );
        std::vector< Tensor< 1, 2 > > vdirs( dirs.size() );
        
        std::copy( pts.begin(), pts.end(), vpts.begin() );
        std::copy( dirs.begin(), dirs.end(), vdirs.begin() );

        write_vecfield_vtk( "matchings_avg.vtk", "matchings_avg", vpts, vdirs );
        write_vecfield_py( "matchings_avg.py", "matchings_avg", vpts, vdirs );

        std::cout << "min/avg/max per cell : " << min_per_cell << " / " << tot_match / double(ncells) << " / " << max_per_cell << std::endl;


        //
        // compute error
        //

        double  error = 0;
        
        for ( uint  i = 0; i < grad_dir.size(); ++ i )
        {
            error += square( ( grad_dir[i] - vdirs[i] ).norm() );
        }// for

        std::cout << "error : " << error << " / " << error / double(ncells) << " / " << error / ( double(ncells) * avg_match_len ) << std::endl;
    }

    {
        std::cout << "using coarse grid" << std::endl;
        
        //
        // use coarse grid with step width coarse_h for averaging gradient and matchings
        //

        uint    ncoarse_cells = uint( std::sqrt( double(ncells) ) );
        double  coarse_h      = cell_len;
        
        if ( ncoarse != 0 )
        {
            coarse_h = ( domain_ub - domain_lb ) / ncoarse;
        }// if

        {
            std::ofstream  out( "grid.py" );

            out << "cell_len = " << cell_len << std::endl;
            out << "coarse_cell_len = " << coarse_h << std::endl;
        }

        std::cout << "coarse_cell_len = " << coarse_h << std::endl;
        
        std::vector< Point< 2 > >     avg_grad_orig;
        std::vector< Tensor< 1, 2 > > avg_grad_dir;
        
        {
            //
            // average gradient within coarse cell
            //
        
            std::deque< Point< 2 > >     pts;
            std::deque< Tensor< 1, 2 > > dirs;
        
            for ( double  coarse_y = domain_lb; coarse_y < domain_ub; coarse_y += coarse_h )
            {
                for ( double  coarse_x = domain_lb; coarse_x < domain_ub; coarse_x += coarse_h )
                {
                    // const double  cell_x[] = { coarse_x, coarse_x + coarse_h };
                    // const double  cell_y[] = { coarse_y, coarse_y + coarse_h };
                    
                    //
                    // determine center of cell
                    //
            
                    const auto  cell_center = Point<2>( coarse_x + coarse_h / 2.0, coarse_y + coarse_h / 2.0 );

                    //
                    // average all matchings originating/ending in cell
                    //

                    Tensor< 1, 2 >  S;
                    double          weight = 0;

                    S[0] = S[1] = 0.0;
            
                    for ( size_t  ng = 0; ng < grad_origin.size(); ++ng )
                    {
                        const auto  O    = grad_origin[ng];
                        const auto  D    = grad_dir[ng];
                        const auto  dist = O - cell_center;
                        const auto  s    = std::exp( - square( 2.0 / coarse_h * dist.norm() ) );
                    
                        S[0]   += s * D[0];
                        S[1]   += s * D[1];
                        weight += s;
                    }// for

                    if ( weight > 0 )
                    {
                        S[0] /= weight;
                        S[1] /= weight;
                    }// if

                    pts.push_back( cell_center );
                    dirs.push_back( S );
                }// for
            }// for

            avg_grad_orig.resize( pts.size() );
            avg_grad_dir.resize( pts.size() );
            
            std::copy( pts.begin(), pts.end(), avg_grad_orig.begin() );
            std::copy( dirs.begin(), dirs.end(), avg_grad_dir.begin() );

            write_vecfield_vtk( "gradient_avg_coarse.vtk", "gradient_avg_coarse", avg_grad_orig, avg_grad_dir );
            write_vecfield_py( "gradient_avg_coarse.py", "gradient_avg_coarse", avg_grad_orig, avg_grad_dir );
        }

        std::vector< Point< 2 > >     avg_match_orig;
        std::vector< Tensor< 1, 2 > > avg_match_dir;
        
        {
            //
            // average matchings within cell
            //
        
            std::deque< Point< 2 > >     pts;
            std::deque< Tensor< 1, 2 > > dirs;
        
            for ( double  coarse_y = domain_lb; coarse_y < domain_ub; coarse_y += coarse_h )
            {
                for ( double  coarse_x = domain_lb; coarse_x < domain_ub; coarse_x += coarse_h )
                {
                    //
                    // determine center of cell
                    //
            
                    const auto  cell_center = Point<2>( coarse_x + coarse_h / 2.0, coarse_y + coarse_h / 2.0 );

                    //
                    // average all matchings originating/ending in cell
                    //
                    // Σ_i (( w(|X_i-C|) + w(|Y_i-C|) )·(X_i-Y_i))
                    // ───────────────────────────────────────────
                    //    Σ_i ( w(|X_i-C|) + w(|Y_i-C|) )
                    //

                    Tensor< 1, 2 >  S;
                    double          weight = 0;

                    S[0] = S[1] = 0.0;
            
                                
                    for ( size_t  nm = 0; nm < matching_data.size(); ++nm )
                    {
                        const auto  X = Point< 2 >{ matching_data[nm].x0, matching_data[nm].x1 };
                        const auto  Y = Point< 2 >{ matching_data[nm].y0, matching_data[nm].y1 };
                        const auto  D = Y - X;

                        {
                            const auto  dist = X - cell_center;
                            const auto  s    = std::exp( - square( 2.0 / coarse_h * dist.norm() ) );
                    
                            S[0] += s * D[0];
                            S[1] += s * D[1];
                            weight += s;
                        }

                        {
                            const auto  dist = Y - cell_center;
                            const auto  s    = std::exp( - square( 2.0 / coarse_h * dist.norm() ) );
                    
                            S[0] += s * D[0];
                            S[1] += s * D[1];
                            weight += s;
                        }
                    }// for

                    if ( weight > 0 )
                    {
                        S[0] /= weight;
                        S[1] /= weight;
                    }// if

                    pts.push_back( cell_center );
                    dirs.push_back( S );
                }// for
            }// for

            avg_match_orig.resize( pts.size() );
            avg_match_dir.resize( dirs.size() );
        
            std::copy( pts.begin(), pts.end(), avg_match_orig.begin() );
            std::copy( dirs.begin(), dirs.end(), avg_match_dir.begin() );

            write_vecfield_vtk( "matchings_avg_coarse.vtk", "matchings_avg_coarse", avg_match_orig, avg_match_dir );
            write_vecfield_py( "matchings_avg_coarse.py", "matchings_avg_coarse", avg_match_orig, avg_match_dir );
        }

        //
        // compute error
        //

        double  error = 0;
        
        for ( uint  i = 0; i < avg_grad_dir.size(); ++ i )
        {
            error += square( ( avg_grad_dir[i] - avg_match_dir[i] ).norm() );
        }// for

        std::cout << "error : " << error << " / " << error / double(ncoarse_cells) << " / " << error / ( double(ncoarse_cells) * avg_match_len ) << std::endl;
    }
}

void
Problem::compute_gradient ( const Vector< double > &         phi,
                            std::vector< Tensor< 1, 2 > > &  gradient,
                            std::vector< Point< 2 > > &      dof_points,
                            const gradient_method_t          method ) const
{
    std::deque< Point< 2 > >      my_points;
    std::deque< Tensor< 1, 2 > >  my_grads;
    
    if ( method == at_cell_center )
    {
        //
        // eval gradient at center of cell
        //
        
        const auto           X_unit = Point< 2 >{ 0.5, 0.5 };
        const Quadrature<2>  quadrature( X_unit );
        FEValues<2>          fe_values( *fe, quadrature, update_gradients | update_quadrature_points );
        auto                 gradients = std::vector< Tensor< 1, 2 > >( 1 );
            
        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            fe_values.reinit( cell );
            fe_values.get_function_gradients( phi, gradients );

            my_points.push_back( fe_values.quadrature_point( 0 ) );
            my_grads.push_back( gradients[ 0 ] );
        }// for
    }// if
    else if ( method == cell_average )
    {
        //
        // average gradient in cell drawn at center of cell
        //
        
        const auto           X_unit = Point< 2 >{ 0.5, 0.5 };
        const Quadrature<2>  quadrature( X_unit );
        FEValues<2>          fe_values( *fe, quadrature, update_gradients | update_quadrature_points );
        auto                 gradients = std::vector< Tensor< 1, 2 > >( 1 );
            
        QGaussChebyshev<2>   vertex_quadrature( 4 );
        FEValues< 2 >        fe_values2( *fe, vertex_quadrature, update_gradients | update_quadrature_points );
        const auto           n_q_points = vertex_quadrature.size();
        auto                 gradients2  = std::vector< Tensor< 1, 2 > >( n_q_points );
    
        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            fe_values.reinit( cell );
            fe_values.get_function_gradients( phi, gradients );

            {
                fe_values2.reinit( cell );
                fe_values2.get_function_gradients( phi, gradients2 );

                gradients[0][0] = 0;
                gradients[0][1] = 0;
                
                for ( uint q_point = 0; q_point < n_q_points; ++q_point )
                {
                    gradients[0][0] += gradients2[ q_point ][0];
                    gradients[0][1] += gradients2[ q_point ][1];
                }// for

                gradients[0][0] /= double( n_q_points );
                gradients[0][1] /= double( n_q_points );
            }
            
            my_points.push_back( fe_values.quadrature_point( 0 ) );
            my_grads.push_back( gradients[ 0 ] );
        }// for
    }// if
    else if ( method == at_matching_points )
    {
        //
        // gradient at matching points
        //
        
        auto  gradients = std::vector< Tensor< 1, 2 > >( 1 );
        auto  mapping   = StaticMappingQ1< 2 >::mapping;
    
        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            for ( size_t  nm = 0; nm < matching_data.size(); ++nm )
            {
                const auto  X = Point< 2 >{ matching_data[nm].x0, matching_data[nm].x1 };

                if ( cell->point_inside( X ) )
                {
                    const auto           X_unit = mapping.transform_real_to_unit_cell( cell, X );
                    const Quadrature<2>  quadrature( GeometryInfo<2>::project_to_unit_cell( X_unit ) );
                    FEValues<2>          fe_values( *fe, quadrature, update_gradients | update_quadrature_points );
                
                    fe_values.reinit( cell );
                    fe_values.get_function_gradients( phi, gradients );
                
                    my_points.push_back( fe_values.quadrature_point( 0 ) );
                    my_grads.push_back( gradients[ 0 ] );
                }// if
            }// for
        }// for
    }// if
    else if ( method == at_quadrature_points )
    {
        //
        // gradients at quadrature points
        //
        
        // const QTrapezoid< 2 >  vertex_quadrature;
        // QGauss<2>       vertex_quadrature( 4 );
        QGaussChebyshev<2>  vertex_quadrature( 1 );
        FEValues< 2 >       fe_values( *fe, vertex_quadrature, update_gradients | update_quadrature_points );
    
        const auto  n_q_points = vertex_quadrature.size();
        auto        gradients  = std::vector< Tensor< 1, 2 > >( n_q_points );
    
        for ( const auto &  cell : dof_handler.active_cell_iterators() )
        {
            fe_values.reinit( cell );
            fe_values.get_function_gradients( phi, gradients );

            for ( uint q_point = 0; q_point < n_q_points; ++q_point )
            {
                my_points.push_back( fe_values.quadrature_point( q_point ) );
                my_grads.push_back( gradients[ q_point ] );
            }// for
        }// for
    }// if
    
    dof_points.resize( my_points.size() );
    gradient.resize( my_grads.size() );

    std::copy( my_points.begin(), my_points.end(), dof_points.begin() );
    std::copy( my_grads.begin(), my_grads.end(), gradient.begin() );
}

int
main ( int argc, char ** argv )
{
    uint         lvl       = 3;
    uint         order     = 1;
    std::string  matchings = "matchings.dat";
    std::string  normals   = "normals.dat";
    
    //
    // parse command line options
    //

    options_description  opts( "usage: compgrid [options]\n  where options include" );
    variables_map        vm;

    opts.add_options()
        ( "help,h",                            ": print this help text" )
        ( "matchings,m", value<std::string>(), ": matching data file" )
        ( "normals,n",   value<std::string>(), ": normal data file" )
        ( "level,l",     value<uint>(),        ": refinement levels" )
        ( "order,o",     value<uint>(),        ": order of FE elements" )
        ( "weighted,w",                        ": use weighted matchings" )
        ( "coarse,c",    value<uint>(),        ": number of cells per axis of coarse grid" )
        ;

    try
    {
        store( command_line_parser( argc, argv ).options( opts ).run(), vm );
        notify( vm );
    }// try
    catch ( required_option &  e )
    {
        std::cout << e.get_option_name() << " requires an argument, try \"-h\"" << std::endl;
        exit( 1 );
    }// catch
    catch ( unknown_option &  e )
    {
        std::cout << e.what() << ", try \"-h\"" << std::endl;
        exit( 1 );
    }// catch

    if ( vm.count( "help") )
    {
        std::cout << opts << std::endl;
        exit( 1 );
    }// if

    if ( vm.count( "matchings" ) ) matchings = vm["matchings"].as<std::string>();
    if ( vm.count( "normals"   ) ) normals   = vm["normals"].as<std::string>();
    if ( vm.count( "level"     ) ) lvl       = vm["level"].as<uint>();
    if ( vm.count( "order"     ) ) order     = vm["order"].as<uint>();
    if ( vm.count( "weighted"  ) ) weighted_matching = true;
    if ( vm.count( "coarse"    ) ) ncoarse   = vm["coarse"].as<uint>();

    //
    // solve PDE
    //
    
    FE_Q<2>  fe( order );
    Problem  problem( fe );

    read_matching_data( matchings );
    read_normal_data( normals );
    
    problem.run( lvl );

    return 0;
}
