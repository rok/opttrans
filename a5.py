#!/usr/bin/env python3
#
# ffmpeg -r 1 -f image2 -pattern_type glob -i 'a5_*.png' -vcodec libx264 -crf 10 -pix_fmt yuv420p a5.mp4
#

import pdb

from common import *

size   = [ 1000, 1000 ]
center = [ size[0] / 2, size[1] / 2 ]

seed = random.randint( 0, 2147483647 )
seed = 0
print( "seed = %d" % seed )
np.random.seed( seed )

# problem size
L = 100

#############################################################

px = 4
py = 4

N = [[]] * px

for i in range( px ) :
    N[i] = [[]] * py

ndraw = 50

#
# iterate over several realizations
#
for niter in range( 1, 10000 ) :

    n = int( np.random.poisson( L*L ) )

    print( "setting up 2 × %d samples" % n )

    surface = None
    draw    = None
    
    if niter % ndraw == 1 :

        surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
        draw    = cairo.Context( surface )

        # determine text offsets
        draw.save()
        draw.set_font_size( L / 10 )
        draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
        xbearing, ybearing, width, height, dx, dy = draw.text_extents( "9999" )
        xofs =  L/100
        yofs = -L/100
        draw.restore()
    
        set_color( draw, Tango['White'] )
        draw.rectangle( 0, 0, size[0], size[1] )
        draw.fill()
        draw.rectangle( 0, 0, size[0], size[1] )
        draw.clip()

        draw.set_line_width( 0.1 )
        draw.translate( size[0] * 0.05, size[1]*0.05 )
        draw.scale( size[0] / (1.1*L), size[0] / (1.1*L) )

        # draw.rectangle( -1, -1, L+2, L+2 )
        # draw.clip()
    
        draw.save()
    
        set_color( draw, Tango['Black'] )
        draw.set_line_width( L / 500 )
        draw.rectangle( 0, 0, L, L )
        draw.stroke()

        for i in range(1,px) :
            draw.move_to( 0, L/px * i )
            draw.line_to( L, L/px * i )
            draw.stroke()

        for j in range(1,py) :
            draw.move_to( L/px * j, 0 )
            draw.line_to( L/px * j, L )
            draw.stroke()

        draw.restore()
    
    #
    # set up random coordinates and count point per quadrant
    #

    n_tot = [[]] * px
    
    for i in range( px ) :
        n_tot[i] = [0] * py
    
    for i in range( n ) :
        
        x = np.random.uniform( 0, L )
        y = np.random.uniform( 0, L )

        for i in range(px) :
            lbi = L/px * i
            ubi = lbi + L/px

            if x < lbi or x >= ubi :
                continue

            for j in range(py) :
                lbj = L/px * j
                ubj = lbj + L/px

                if y < lbj or y >= ubj :
                    continue

                n_tot[i][j] += 1
                break
            
        if niter % ndraw == 1 :
            set_color( draw, Tango['SkyBlue3'] )
            draw.arc( x, y, L / 500, 0, 2*pi )
            draw.fill()

    for i in range(px) :
        for j in range(py) :
            N[i][j] = N[i][j] + [ n_tot[i][j] ]
    
    if niter % ndraw == 1 :
        draw.save()
    
        draw.set_font_size( L / 20 )
        draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )

        for i in range(px) :
            for j in range(py) :
                text = "%.2f" % ( n_tot[i][j] / (L*L/(px*py)) )
                xbearing, ybearing, width, height, dx, dy = draw.text_extents( text )
                set_color_alpha( draw, Tango['White'], 0.8 )
                draw.rectangle( L/px*i - 0.1*width + xofs, L/px*(j+1) + yofs - 1.1*height, 1.21*width, 1.21*height )
                draw.fill()
                
                set_color( draw, Tango['ScarletRed3'] )
                draw.move_to( L/px*i + xofs, L/px*(j+1) + yofs )
                draw.show_text( text )
    
        draw.restore()

        n_sum = [[]] * px
        n_avg = [[]] * px
        n_var = [[]] * px
        
        for i in range(px) :
            n_sum[i] = [0] * py
            n_avg[i] = [0] * py
            n_var[i] = [0] * py
            
            for j in range(py) :
                n_sum[i][j] = sum( N[i][j] )
                n_avg[i][j] = n_sum[i][j] / float(niter)
                n_var[i][j] = ( 1.0 / niter ) * sum( [ ( ni - n_avg[i][j] )**2 for ni in N[i][j] ] )

        # print( n_sum )
        # print( n_avg )
        # print( n_var )
        
        draw.save()
            
        draw.set_font_size( L / 20 )
        draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )

        for i in range(1) :
            for j in range(1) :
                
                xbearing, ybearing, width, height, dx, dy = draw.text_extents( "0%d" % int(n_var[i][j]) )
                set_color_alpha( draw, Tango['White'], 0.8 )
                draw.rectangle( L/px*(i+1) - 1.1*width - xofs, L/px*(j) - yofs, 1.15*width, 1.21*height )
                draw.fill()
                
                set_color( draw, Tango['Orange3'] )
                draw.move_to( L/px*(i+1) - width, L/px*(j) - yofs + height )
                draw.show_text( "%d" % int(n_var[i][j]) )

        draw.restore()

        draw.save()
        
        text = "%.0f" % float(L/4)
        draw.set_font_size( L / 30 )
        draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
        xbearing, ybearing, width, height, dx, dy = draw.text_extents( text )
        set_color( draw, Tango['SkyBlue3'] )
        draw.move_to( 0.5*L/px - 0.5*width, 0.5*yofs )
        draw.show_text( text )
        draw.move_to( 0, 0 )
        draw.rotate( -1.570796326794897 )
        draw.move_to( -0.5*L/px - 0.5*width, 0.5*yofs )
        # draw.move_to( 0 - width - xofs, 0.5*L/px - 0.5*height )
        draw.show_text( text )
                
        draw.restore()

        surface.write_to_png( "a5_%04d.png" % niter )
        surface.finish()
