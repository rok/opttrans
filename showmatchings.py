#!/usr/bin/env python3

import pdb

import sys
import numpy as np
import pickle

from math import sqrt

filename = 'data.dat'

if len( sys.argv ) > 1 :
    filename = sys.argv[1]
    
datafile = open( filename, 'rb' )

L = pickle.load( datafile )
n = pickle.load( datafile )
X = pickle.load( datafile )
Y = pickle.load( datafile )

matchings = pickle.load( datafile )
crossing  = pickle.load( datafile )
cxpoints  = pickle.load( datafile )
normval   = pickle.load( datafile )
normpos   = pickle.load( datafile )

POS_BOTTOM = 0
POS_RIGHT  = 1
POS_TOP    = 2
POS_LEFT   = 3

lb = 1*L/4
ub = 3*L/4

# from gradient import gradient

# decompose into components
Xx = [ x[0] for x in X ]
Xy = [ x[1] for x in X ]
 
Yx = [ y[0] for y in Y ]
Yy = [ y[1] for y in Y ]

#
# determine average length of matchings
#

avg_d = 0

for pair in matchings :
    dx = Yx[pair[1]] - Xx[pair[0]]
    dy = Yy[pair[1]] - Xy[pair[0]]
    d  = sqrt( dx*dx + dy*dy )
    avg_d += d

avg_d = avg_d / len(matchings)

print( "average matching length: %.4e" % avg_d )

# #
# # determine average distance between points in X/Y
# #

# avg_d = 0

# for x in X :
#     min_d = L
#     for y in Y :
#         dx = y[0] - x[0]
#         dy = y[1] - x[1]
#         d  = sqrt( dx*dx + dy*dy )
#         min_d = min( [ min_d, d ] )

#     avg_d += min_d

# avg_d = avg_d / len(X)

# print( "average point distance: %.4e" % avg_d )

#
# import matplotlib
#

import matplotlib as mpl

theme = { 'text.usetex'           : False,
          'font.family'           : 'Roboto Condensed',
          # 'figure.facecolor'      : "#fdfdf6",
          'axes.titlesize'        : 16,
          'axes.titlelocation'    : 'center',
          'axes.labelsize'        : 18,
          'axes.labelcolor'       : "#000000",
          'lines.markeredgewidth' : 0.0,
          'lines.markersize'      : 0,
          'lines.markeredgecolor' : 'none',
          'lines.linewidth'       : 3,
          'legend.fontsize'       : 16,
          'legend.framealpha'     : 1.0,
          'xtick.labelsize'       : 14,
          'ytick.labelsize'       : 18,
          'figure.figsize'        : ( 10, 10 )
         }

colors = [ '#888a85',
           '#3465a4',
           '#cc0000',
           '#4e9a06',
           '#f57900',
           '#75507b',
           '#c17d11' ]

line_styles = [ 'solid', 'dashed', 'dotted', 'dashdot' ]

mpl.use('Agg')
mpl.rcParams.update( theme  )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages

nfig = 1

#
# plot matchings in 2D
#

fig = plt.figure( nfig, figsize = (10, 10) )
nfig += 1

ax  = plt.subplot( 1, 1, 1 )
 
# show X and Y points
# ax.scatter( Xx, Xy, color = colors[1], s = 40, alpha = 0.2, edgecolors = 'none' )
# ax.scatter( Yx, Yy, color = colors[2], s = 40, alpha = 0.2, edgecolors = 'none' )

# coordinates for crossing points
for i  in range(len(crossing)) :
    pair = crossing[i]
    cXx = Xx[pair[0]]
    cXy = Xy[pair[0]]
    cYx = Yx[pair[1]]
    cYy = Yy[pair[1]]
    cU  = Yx[pair[1]] - Xx[pair[0]]
    cV  = Yy[pair[1]] - Xy[pair[0]]
    cW  = 0.05 * sqrt(cU*cU + cV*cV)
    ax.quiver( [ cXx ], [ cXy ], [ cU ], [ cV ], units = 'xy', scale = 1.0, width = cW, alpha = 1.0, color = colors[0] )
    ax.scatter( [ cXx ], [ cXy ], color = colors[1], s = 40, alpha = 1.0, edgecolors = 'none' )
    ax.scatter( [ cYx ], [ cYy ], color = colors[2], s = 40, alpha = 1.0, edgecolors = 'none' )

# show matchings
for pair in matchings :
    ax.plot( [ Xx[pair[0]], Yx[pair[1]] ], [ Xy[pair[0]], Yy[pair[1]] ], linewidth = 0.5, alpha = 0.2, color = '#000000' )
    
# show coordinates
# ax.scatter( Xx, Xy, Xz, color = colors[1] )
# ax.scatter( Yx, Yy, Yz, color = colors[2] )

# plot LxL square
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], linewidth = 2, color = '#000000' )
ax.plot( [ lb, ub, ub, lb, lb ], [ lb, lb, ub, ub, lb ], linewidth = 1, color = colors[0] )

# turn off/on axis
plt.axis('off')
plt.tight_layout()

pp = PdfPages( 'matchings.pdf' )
pp.savefig()
pp.close()

#
# plot matchings in 3D
#

Xz = [ 0 for x in X ]
Yz = [ 1 for y in Y ]

fig = plt.figure( nfig, figsize = (10, 10) )
nfig += 1
 
# Generating a 3D sine wave
ax = plt.axes( projection = '3d' )

ax.view_init( 10, -10 )

# show matchings
for match in matchings :
    x = match[0]
    y = match[1]
    ax.plot( [ X[x][0], Y[y][0] ], [ X[x][1], Y[y][1] ], [ 0, 1 ], color = colors[0], linewidth = 0.5 )

# show coordinates
ax.scatter( Xx, Xy, Xz, color = colors[1] )
ax.scatter( Yx, Yy, Yz, color = colors[2] )

# plot LxL square
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ 0, 0, 0, 0, 0 ], color = '#000000' )
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ 1, 1, 1, 1, 1 ], color = '#000000' )

# trun off/on axis
plt.axis( 'off' )
plt.tight_layout()

plt.savefig( "matchings.png" )

#
# plot normals
#

fig = plt.figure( nfig, figsize = (10, 10) )
nfig += 1

ax  = plt.subplot( 1, 1, 1 )
 
for i  in range(len(crossing)) :
    nu = 0
    nv = 0
    if   normpos[i] == POS_BOTTOM : nv = -normval[i]
    elif normpos[i] == POS_RIGHT  : nu =  normval[i]
    elif normpos[i] == POS_TOP    : nv =  normval[i]
    elif normpos[i] == POS_LEFT   : nu = -normval[i]
        
    ax.quiver( [ cxpoints[i][0] ], [ cxpoints[i][1] ], [ nu ], [ nv ], units = 'xy', scale = 1.0, width = 0.05 * abs(normval[i]), alpha = 1.0, color = colors[5] )

# plot LxL square
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], linewidth = 2, color = '#000000' )
ax.plot( [ lb, ub, ub, lb, lb ], [ lb, lb, ub, ub, lb ], linewidth = 1, color = colors[0] )

# turn off/on axis
plt.axis('off')
plt.tight_layout()

pp = PdfPages( 'normals.pdf' )
pp.savefig()
pp.close()
