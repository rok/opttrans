Optimal Transport
=================

## opttrans.py

Compute optimal matchings for random point sets X and Y in LxL and
matchings crossing inner domain at [1/4 L .. 3/4 L] x [1/4 L .. 3/4 L].

Required: [Python Optimal Transport](https://pythonot.github.io/)

~~~
pip install POT
~~~

Call by

~~~
opttrans.py <L> <seed>
~~~

with `L` and `seed` being optional.

Output:

  - `matchings.dat`: coordinates of all matching pairs plus matching weight
  - `normals.dat`:   coordinates and direction of normals due to matchings
                   crossing inner domain

Both needed as input to `compgrad`.

## compgrad

Computes gradient for solution of PDE defined by optimal matching.

Required: [deal.II](https://www.dealii.org/)

Edit `SConstruct` and changed path to deal.II. Afterwars call `scons` to build `compgrad`.

Call by

~~~
compgrad -l <lvl> -m <matchings.dat> -n <normals.dat> -o <order> -c <ncoarse>
~~~

With `lvl` being the refinement level, `order` the FE order and files from `opttrans.py`. With `ncoarse` the number of
coarse cells per axis is defined.

Output as VTK files for ParaView or VisIt:

  - `gradient.py`: gradient field evaluated at fine cell centers
  - `gradient_avg_coarse.py`: weighted gradient field evaluated at coarse cell centers
  - `matchings_avg.py`: weighted, average matchings at fine cell centers
  - `matchings_avg_coarse.py`: weighted matchings at coarse cell centers
  - `grid.py`: grid used for PDE
  - `solution.vtk`: solution of PDE

The output is used within the `showgradient.py` script.

## Visualizations

### A1 (a1.mp4)

Show optimization process by pairwise exchanging matching partners until minimum is reached 
(plus an additional change to show that it is again getting worse).

Run:

~~~
rm a1_*.png
python3 a1.py
ffmpeg -r 0.5 -f image2 -pattern_type glob -i 'a1_*.png' -vcodec libx264 -crf 10 -pix_fmt yuv420p a1.mp4
~~~

### A2

#### A2_1 (a2_1.mp4)

Randomly choose points in $`L \times L`$ domain.

Run:

~~~
rm a2_*.png
python3 a2_1.py
ffmpeg -r 10 -f image2 -i "a2_%04d.png" -vcodec libx264 -crf 10 -pix_fmt yuv420p a2.mp4
~~~

#### A2_2 (a2_2.mp4)

Show probability distribution for points in a subdomain of $`L \times L`$ with many realizations.

~~~
rm a2_*.png
python3 a2_2.py
ffmpeg -r 25 -f image2 -i "a2_%04d.png" -vcodec libx264 -crf 10 -pix_fmt yuv420p a2.mp4
~~~

### A3 (a3.pdf)

Show 1D matching by choosing start match in the middle and then succesively choosing the next pair on either side for
the next match.

~~~
python3 a3.py
~~~

### A4 (a4.png)

3D visualization of matchings. The program uses data from `opttrans.py`. By default the file `data.dat`. Alternatively,
the data file may be defined as a command line argument.

~~~
python3 a3.py data.dat
~~~

### A5 (a5.mp4)

Show density per subdomain for many realizations of random points.

~~~
rm a5_*.png
python3 a5.py
ffmpeg -r 25 -f image2 -i "a2_%04d.png" -vcodec libx264 -crf 10 -pix_fmt yuv420p a2.mp4
~~~

### B1 (b1.png)

3D visualization of matchings crossing inner boundary. The program uses data from `opttrans.py`. By default the file
`data.dat`. Alternatively, the data file may be defined as a command line argument.

~~~
python3 b1.py data.dat
~~~
