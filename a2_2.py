#!/usr/bin/env python3
#
# ffmpeg -r 25 -f image2 -i "a2_%04d.png" -vcodec libx264 -crf 10 -pix_fmt yuv420p a2_2.mp4
#

import scipy.special as sp

from common import *

seed = random.randint( 0, 2147483647 )
seed = 0
print( "seed = %d" % seed )
np.random.seed( seed )

size = [ 1000, 1000 ]

def init_surface ( size, L, n ) :
    surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
    draw    = cairo.Context( surface )

    set_color( draw, Tango['White'] )
    draw.rectangle( 0, 0, size[0], size[1] )
    draw.fill()

    draw.set_line_width( 0.1 )
    draw.translate( size[0] * 0.05, size[1]*0.05 )
    draw.scale( size[0] / (1.1*L), size[0] / (1.1*L) )

    draw.rectangle( -1, -1, L+2, L+2 )
    draw.clip()

    set_color( draw, Tango['Black'] )
    draw.set_line_width( L / 500 )
    draw.rectangle( 0, 0, L, L )
    draw.stroke()

    draw.save()
    draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
    draw.set_font_size( L / 20 )
    text = "L = %d" % L
    xbearing, ybearing, width, height, dx, dy = draw.text_extents( text )
    draw.move_to( L - width, -height / 5 )
    draw.show_text( text )
    draw.restore()

    return surface, draw

#############################################################

#
# problem size
#

# np.random.seed( seed = 0 )

L = 10

n = int( np.random.poisson( L*L ) )

print( "setting up 2 × %d samples" % n )

L_distr = n / 25
X_distr = np.arange( 0, n )
P_distr = -L_distr + X_distr * np.log( L_distr ) - sp.gammaln( X_distr + 1 )
P_distr = np.exp( P_distr )

#
# many realisations and counter points in predefined section
#

lb = 0.3 * L
ub = 0.5 * L

navg = 0
nexp = (( ub - lb ) / L )**2 * n

print( "expected value : ", nexp )

hist_vals   = [ 0 ] * n
hist_xscale = 5.0*L / n
hist_yscale = 2*L
hist_pos    = [ L/50 + 0.5, L - L/50 ]

start  = 7
stride = 100
nfig   = 0

niter = 2000

for iter_idx in range( niter ) :

    surface, draw = init_surface( size, L, n )

    X = []
    Y = []
    
    nsection = 0
    for i in range( n ) :

        x = np.random.uniform( 0, L )
        y = np.random.uniform( 0, L )

        if iter_idx > start or ( iter_idx > 10 and iter_idx % stride == 0 ) :
            X.append( x )
            Y.append( y )
            
        if x >= lb and x <= ub and y >= lb and y <= ub :
            nsection += 1

    hist_vals[ nsection ] += 1

    if iter_idx > start or ( iter_idx > 10 and iter_idx % stride == 0 ) :
        for i in range( n ) :
            x = X[i]
            y = Y[i]

            if x >= lb and x <= ub and y >= lb and y <= ub :
                set_color( draw, Tango['SkyBlue3'] )
            else :
                set_color( draw, Tango['Aluminium2'] )

            draw.arc( x, y, L / 200, 0, 2*pi )
            draw.fill()
            
        draw.save()
        set_color( draw, Tango['ScarletRed3'] )
        draw.set_line_width( L / 200 )
        draw.rectangle( lb, lb, ub-lb, ub-lb )
        draw.stroke()
        draw.restore()

        draw.save()
        set_color( draw, Tango['Black'] )
        draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
        draw.set_font_size( L / 40 )
        xbearing, ybearing, width, height, dx, dy = draw.text_extents( "%d" % nsection )
        draw.move_to( ub - L/80 - width, ub-height )
        draw.show_text( "%d" % nsection )
        draw.restore()

        nhist = 0
        for h in hist_vals :
            nhist += h

        hist_prob = [0] * len(hist_vals)
        for i in range(len(hist_vals)) :
            hist_prob[i] = float(hist_vals[i]) / float(nhist)

        # print( hist_prob[0:20] )
        draw.save()
        set_color( draw, Tango['ScarletRed1'] )
        for j in range( 0, n ) :
            if hist_vals[j] != 0 :
                # draw.set_line_width( L / 100 )
                draw.move_to( hist_pos[0] + hist_xscale * (j+0.5), hist_pos[1] )
                draw.line_to( hist_pos[0] + hist_xscale * (j+0.5), hist_pos[1] - hist_yscale * hist_prob[j] )
                draw.line_to( hist_pos[0] + hist_xscale * (j+1.5) - 1, hist_pos[1] - hist_yscale * hist_prob[j] )
                draw.line_to( hist_pos[0] + hist_xscale * (j+1.5) - 1, hist_pos[1] )
                draw.fill()
        draw.restore()

        # draw.save()
        # set_color( draw, Tango['Orange1'] )
        # draw.move_to( hist_pos[0] + hist_xscale * ( nexp - 0.5 ), hist_pos[1] )
        # draw.line_to( hist_pos[0] + hist_xscale * ( nexp - 0.5 ), hist_pos[1] - 0.4 * L )
        # draw.stroke()
        # draw.restore()

        draw.save()
        set_color( draw, Tango['Black'] )
        draw.move_to( hist_pos[0], hist_pos[1] - hist_yscale * P_distr[0] )
        for i in range( 18 ) : # len( P_distr ) ) :
            draw.rectangle( hist_pos[0] + hist_xscale * (i-0.5),
                            hist_pos[1],
                            hist_xscale,
                            -hist_yscale * P_distr[i] )
            draw.stroke()
            # draw.line_to( hist_pos[0] + hist_xscale * i, hist_pos[1] - hist_yscale * P_distr[i] )
        draw.stroke()
        draw.restore()
            
        # text = "μ"
        # ( w, h ) = draw.textsize( text, fn48 )
        # draw.text( ( hist_pos[0] + hist_scale * nexp - w/2, hist_pos[1] + 10 ), text, fill = Tango['Orange1'], font = fn48 )
    
        surface.write_to_png( "a2_%04d.png" % nfig )
        nfig += 1
        surface.finish()

# print( P_distr[0:20] )
