#!/usr/bin/env python3

import pdb
import numpy as np
import ot
import pickle
import sys
import random

from time import time
from math import sqrt

datafile = open( 'data.dat', 'wb' )

#############################################################

#
# problem size
#

# SEEDS
# =====
# 
# L = 100  :  2082388981
#

L    = 50.0
seed = random.randint( 0, 2147483647 )

if len( sys.argv ) > 1 :
    L = int( sys.argv[1] )

if len( sys.argv ) > 2 :
    seed = int( sys.argv[2] )

np.random.seed( seed = seed )
print( "seed = %d" % seed )

n = int( np.random.poisson( L*L ) )
print( "setting up 2 × %d samples for L = %.0f" % ( n, L ) )

pickle.dump( L, datafile )
pickle.dump( n, datafile )

#
# set up random coordinates
#

X = np.zeros( [ n, 2 ] )
Y = np.zeros( [ n, 2 ] )

for i in range( n ) :
    X[i] = [ np.random.uniform( 0, L ), np.random.uniform( 0, L ) ]
    Y[i] = [ np.random.uniform( 0, L ), np.random.uniform( 0, L ) ]
    
# print( X )
pickle.dump( X, datafile )
pickle.dump( Y, datafile )

# uniform distribution on samples
a = np.ones((n,)) / n
b = np.ones((n,)) / n 

# loss matrix
M = ot.dist( X, Y, metric = 'sqeuclidean' )
M /= M.max()

# print( M )

def f(G):
    return 0.5 * np.sum(G**2)


def df(G):
    return G

# compute EMD
tic = time()
G0 = ot.emd( a, b, M, 100000000, numThreads = 8 )
# G0 = ot.sinkhorn( a, b, M, 5e-3, numIterMax = 100000000 )
# G0 = ot.bregman.empirical_sinkhorn( X, Y, 1e-1, numIterMax = 100000000 )
# G0 = ot.optim.cg(a, b, M, 1e-1, f, df, G0 = G0, verbose = True, stopThr = 1e-03, stopThr2 = 1e-04, numItermaxEmd = 100000000, )
toc = time()

matchings = []
weight    = 0

# print( G0[0] )
# print( np.argmax( G0[0] ) )

for x in range( n ) :
    y = np.argmax( G0[x] )
    matchings.append( [ x, y, G0[x,y] * n ] )
    weight += np.linalg.norm( X[x] - Y[y] )

print( "found matchings in %.2fs, weight = %.4e" % ( ( toc - tic ), weight ) )

# plt.figure( nfig, figsize = ( 10, 10 ) )
# nfig += 1
# plt.imshow( G0, interpolation = 'nearest' )
# plt.title( 'OT matrix G0' )
# plt.axis( 'off' )
# plt.tight_layout()

# plt.savefig( "G0.png" )

pickle.dump( matchings, datafile )

#
# show samples
#

# decompose into components
Xx = [ x[0] for x in X ]
Xy = [ x[1] for x in X ]
Xz = [ 0    for x in X ]
 
Yx = [ y[0] for y in Y ]
Yy = [ y[1] for y in Y ]
Yz = [ 1    for y in Y ]

#
# determine average length of matchings
#

avg_d = 0

for pair in matchings :
    dx = Yx[pair[1]] - Xx[pair[0]]
    dy = Yy[pair[1]] - Xy[pair[0]]
    d  = sqrt( dx*dx + dy*dy )
    avg_d += d

avg_d = avg_d / len(matchings)

print( "average matching length: %.4e" % avg_d )

# sys.exit( 0 )

# write matching data for "compgrad"
with open( "matchings.dat", "w" ) as normout :
    for match in matchings :
        x = match[0]
        y = match[1]
        w = match[2]
        normout.write( "%.8e %.8e %.8e %.8e 1.0\n" % ( X[x][0], X[x][1], Y[y][0], Y[y][1] ) );

#
# determine pairs crossing inner boundary
#

lb = 1*L/4
ub = 3*L/4

crossing = []
cxpoints = []
normval  = []
normpos  = []

POS_BOTTOM = 0
POS_RIGHT  = 1
POS_TOP    = 2
POS_LEFT   = 3

for pair in matchings :
    Xi = X[pair[0]]
    Yi = Y[pair[1]]

    x_inner = ( Xi[0] > lb and Xi[0] < ub ) and ( Xi[1] > lb and Xi[1] < ub )
    y_inner = ( Yi[0] > lb and Yi[0] < ub ) and ( Yi[1] > lb and Yi[1] < ub )

    if x_inner != y_inner :

        # print( Xi, " -> ", Yi )
        crossing.append( pair )
    
        #
        # determine intersection with inner square
        #

        d  = Yi - Xi
        u  = d[0]
        v  = d[1]
        x1 = Xi[0]
        x2 = Xi[1]
        found = False

        cxp   = np.array( [ 0, 0 ] )
        pos   = -1
        Len   = L*L
        nval  = 0

        # bottom border
        if not found :
            alpha = ( lb - x2 ) / v
            inter = Xi + alpha * d

            if (( alpha >= 0 ) and ( inter[0] > lb ) and ( inter[0] < ub ) and ( np.linalg.norm( inter - Xi ) < Len )) :
                cxp  = inter
                pos  = POS_BOTTOM
                nval = np.linalg.norm( d )
                Len  = np.linalg.norm( inter - Xi )

        # right border
        if not found :
            alpha = ( ub - x1 ) / u
            inter = Xi + alpha * d

            if (( alpha >= 0 ) and ( inter[1] > lb ) and ( inter[1] < ub ) and ( np.linalg.norm( inter - Xi ) < Len )) :
                cxp  = inter
                pos  = POS_RIGHT
                nval = np.linalg.norm( d )
                Len  = np.linalg.norm( inter - Xi )

        # upper border
        if not found :
            alpha = ( ub - x2 ) / v
            inter = Xi + alpha * d

            if (( alpha >= 0 ) and ( inter[0] > lb ) and ( inter[0] < ub ) and ( np.linalg.norm( inter - Xi ) < Len )) :
                cxp  = inter
                pos  = POS_TOP
                nval = np.linalg.norm( d )
                Len  = np.linalg.norm( inter - Xi )

        # left border
        if not found :
            alpha = ( lb - x1 ) / u
            inter = Xi + alpha * d

            if (( alpha >= 0 ) and ( inter[1] > lb ) and ( inter[1] < ub ) and ( np.linalg.norm( inter - Xi ) < Len )) :
                cxp  = inter
                pos  = POS_LEFT
                nval = np.linalg.norm( d )
                Len  = np.linalg.norm( inter - Xi )

        cxpoints.append( cxp )
        normpos.append( pos )
        if x_inner : normval.append(  nval )
        else       : normval.append( -nval )

print( "found %d crossings" % ( len(crossing) ) )
        
for pair0 in crossing :
    for pair1 in crossing :
        if pair0 != pair1 :
            X0 = X[pair0[0]]
            Y0 = Y[pair0[1]]
            X1 = X[pair1[0]]
            Y1 = Y[pair1[1]]

            if np.dot( X1 - X0, Y1 - Y0 ) < 0 :
                print( "found dot error : ", np.dot( X1 - X0, Y1 - Y0 ) )

pickle.dump( crossing, datafile )
pickle.dump( cxpoints, datafile )
pickle.dump( normval, datafile )
pickle.dump( normpos, datafile )

#
# write normal data for "compgrad"
#

with open( "normals.dat", "w" ) as normout :
    normout.write( "%.8e\n" % lb );
    normout.write( "%.8e\n" % ub );
    for i  in range(len(crossing)) :
        nu = 0
        nv = 0
        sign = 1
        if normval[i] < 0 : sign = -1
        
        if   normpos[i] == POS_BOTTOM : nv = -normval[i]
        elif normpos[i] == POS_RIGHT  : nu =  normval[i]
        elif normpos[i] == POS_TOP    : nv =  normval[i]
        elif normpos[i] == POS_LEFT   : nu = -normval[i]

        normout.write( "%.8e %.8e %.8e %.8e %f\n" % ( cxpoints[i][0], cxpoints[i][1], nu, nv, sign ) );

