#!/usr/bin/env python3
#
# ffmpeg -r 10 -f image2 -i "a2_%04d.png" -vcodec libx264 -crf 10 -pix_fmt yuv420p a2_1.mp4
#

from common import *

seed = random.randint( 0, 2147483647 )
seed = 0
print( "seed = %d" % seed )
np.random.seed( seed )

size = [ 1000, 1000 ]

def init_surface ( size, L, n ) :
    surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
    draw    = cairo.Context( surface )

    set_color( draw, Tango['White'] )
    draw.rectangle( 0, 0, size[0], size[1] )
    draw.fill()

    draw.set_line_width( 0.1 )
    draw.translate( size[0] * 0.05, size[1]*0.05 )
    draw.scale( size[0] / (1.1*L), size[0] / (1.1*L) )

    draw.rectangle( -1, -1, L+2, L+2 )
    draw.clip()

    set_color( draw, Tango['Black'] )
    draw.set_line_width( L / 500 )
    draw.rectangle( 0, 0, L, L )
    draw.stroke()

    draw.save()
    draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
    draw.set_font_size( L / 20 )
    text = "L = %d" % L
    xbearing, ybearing, width, height, dx, dy = draw.text_extents( text )
    draw.move_to( L - width, -height / 5 )
    draw.show_text( text )
    draw.restore()

    return surface, draw

#############################################################

#
# problem size
#

# np.random.seed( seed = 0 )

L = 10

n = int( np.random.poisson( L*L ) )

print( "setting up 2 × %d samples" % n )

#
# fill domain by random points
#

surface, draw = init_surface( size, L, n )
set_color( draw, Tango['SkyBlue3'] )

for i in range( n ) :

    x = np.random.uniform( 0, L )
    y = np.random.uniform( 0, L )

    draw.arc( x, y, L / 200, 0, 2*pi )
    draw.fill()

    surface.write_to_png( "a2_%04d.png" % i )

surface.finish()
