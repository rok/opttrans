#!/usr/bin/env python3

import numpy as np
import ot
import random

from scipy.io import savemat
from time     import time
from math     import sqrt, pi

import cairo

Tango = { 'Butter1' : [ 252,233,79 ],
          'Butter2' : [ 237,212,0 ],
          'Butter3' : [ 196,160,0 ],

          'Chameleon1' : [ 138,226,52 ],
          'Chameleon2' : [ 115,210,22 ],
          'Chameleon3' : [ 78,154,6 ],

          'Orange1' : [ 252,175,62 ],
          'Orange2' : [ 245,121,0 ],
          'Orange3' : [ 206,92,0 ],

          'SkyBlue1' : [ 114,159,207 ],
          'SkyBlue2' : [ 52,101,164 ],
          'SkyBlue3' : [ 32,74,135 ],

          'Plum1' : [ 173,127,168 ],
          'Plum2' : [ 117,80,123 ],
          'Plum3' : [ 92,53,102 ],

          'Chocolate1' : [ 233,185,110 ],
          'Chocolate2' : [ 193,125,17 ],
          'Chocolate3' : [ 143,89,2 ],

          'ScarletRed1' : [ 239,41,41 ],
          'ScarletRed2' : [ 204,0,0 ],
          'ScarletRed3' : [ 164,0,0 ],

          'Aluminium1' : [ 238,238,236 ],
          'Aluminium2' : [ 211,215,207 ],
          'Aluminium3' : [ 186,189,182 ],
          'Aluminium4' : [ 136,138,133 ],
          'Aluminium5' : [ 85,87,83 ],
          'Aluminium6' : [ 46,52,54 ]
         }

def set_color ( draw, color ) :
    draw.set_source_rgb( color[0] / 255.0,
                         color[1] / 255.0,
                         color[2] / 255.0 )
    
#############################################################

#
# problem size
#
# 377724666

seed = random.randint( 0, 2147483647 )
# seed = 1900816838
seed = 1996549262
print( "seed = %d" % seed )
np.random.seed( seed )

L  = 1000.0
nx = int( np.random.poisson( L ) )
ny = int( np.random.poisson( L ) )

print( "setting up 2 × %d/%d samples" % ( nx, ny ) )

#
# set up random coordinates in [ 0, L ]
#

X = [ 0 ] * nx
Y = [ 0 ] * ny

for i in range( nx ) :
    X[i] = np.random.uniform( 0, 4*L ) - 2*L

X = sorted( X )
# print( X )

for i in range( ny ) :
    Y[i] = np.random.uniform( 0, 4*L ) - 2*L

Y = sorted( Y )
# print( Y )

#
# determine matching by choosing first pair closest at L/2
#

x_min = 0
d_min = abs( X[x_min] )

for i in range(nx) :
    d = abs( X[i] )
    if d < d_min :
        x_min = i
        d_min = d

y_min = 0
d_min = abs( Y[y_min] )
for i in range(ny) :
    d = abs( Y[i] )
    if d < d_min :
        y_min = i
        d_min = d

print( "first matching: ( %d / %f, %d / %f )" % ( x_min, abs( X[x_min] ) - L/2,  y_min, abs( Y[y_min] ) - L/2 ) )

matchings = [ [ x_min, y_min ] ]

x_next = x_min-1
y_next = y_min-1

while x_next >= 0 and y_next >= 0 and ( X[x_next] > -L or Y[y_next] > -L ) :
    matchings.insert( 0, [ x_next, y_next ] )
    x_next -= 1
    y_next -= 1
    
x_next = x_min+1
y_next = y_min+1

while x_next < nx and y_next < ny and ( X[x_next] < L or Y[y_next] < L ) :
    matchings.append( [ x_next, y_next ] )
    x_next += 1
    y_next += 1

print( "found %d matchings" % len( matchings ) )

# print( matchings )

#
# show samples
#

L = 2 * L

size    = ( 2000, 500 )
# surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
surface = cairo.PDFSurface( "a3.pdf", size[0], size[1] )
draw    = cairo.Context( surface )

set_color( draw, [255,255,255] )
draw.rectangle( 0, 0, size[0], size[1] )
draw.fill()

draw.set_line_width( 0.1 )
draw.translate( size[0] * 0.05, size[1] - size[1]*0.1 )
draw.scale( size[0] / (1.1*L), -size[0] / (1.25*L) )
draw.translate( L/2, 0 )

sx = 0.93*0.25*L
sy = 1.02*0.25*L

draw.save()
draw.rectangle( -L/2, sx - 0.05*L, L, sy-sx + 0.1*L )
draw.clip()

# show all matchings
for match in matchings :
    x = match[0]
    y = match[1]

    set_color( draw, Tango['Aluminium5'] )
    draw.move_to( X[x], sx )
    draw.line_to( Y[y], sy )
    draw.stroke()

for x in X :
    set_color( draw, Tango['SkyBlue3'] )
    draw.arc( x, sx, L / 500, 0, 2*pi )
    draw.fill()
    
for y in Y :
    set_color( draw, Tango['ScarletRed3'] )
    draw.arc( y, sy, L / 500, 0, 2*pi )
    draw.fill()

draw.restore()

#
# draw zoom area to the right
#

# draw.save()
# draw.set_line_width( L / 500 )
# set_color( draw, Tango['Orange1'] )
# draw.translate( L*42.5/100, 0 )
# draw.move_to( 0, 1.13*sx )
# draw.line_to( 0, 0.87*sy )
# draw.line_to( 0, -0.05*sy )
# draw.stroke()
# draw.restore()

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.translate( L*42.5/100, 0 )
draw.move_to( -L*2.5/100, 1.13*sx )
draw.line_to( -L*2.5/100, 0.87*sy )
draw.line_to( -2.5*L*2.5/100, 0.8*sy )
draw.line_to( -2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.translate( L*42.5/100, 0 )
draw.move_to( L*2.5/100, 1.13*sx )
draw.line_to( L*2.5/100, 0.87*sy )
draw.line_to( 2.5*L*2.5/100, 0.8*sy )
draw.line_to( 2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()
    
draw.save()

draw.rectangle( L*42.5/100 - 2.5*L*2.5/100, -0.1*sy, 2.5*L*5/100, 0.9*sy )
draw.clip()

# draw.save()
# draw.set_line_width( L/1000 )
# set_color( draw, Tango['Plum3'] )
# draw.move_to( L*42.5/100, 0 )
# draw.line_to( L*42.5/100, L/3 )
# draw.stroke()
# draw.restore()

draw.scale( 2.5, 2.5 )
draw.translate( -L*25.5/100, 0 )

# draw.save()
# draw.set_line_width( L/1000 )
# set_color( draw, Tango['Chameleon3'] )
# draw.move_to( L*42.5/100, 0 )
# draw.line_to( L*42.5/100, L/3 )
# draw.stroke()
# draw.restore()

sx = 0.0 * 0.25 * L
sy = 0.3 * 0.25 * L

# show matchings aways from boundary
for match in matchings :
    x = match[0]
    y = match[1]

    set_color( draw, Tango['Aluminium5'] )
    draw.move_to( X[x], sx )
    draw.line_to( Y[y], sy )
    draw.stroke()

    set_color( draw, Tango['SkyBlue3'] )
    draw.arc( X[x], sx, L/500, 0, 2*pi )
    draw.fill()
    
    set_color( draw, Tango['ScarletRed3'] )
    draw.arc( Y[y], sy, L/500, 0, 2*pi )
    draw.fill()

draw.restore()

#
# draw zoom area in the middle
#

sx = 0.95*0.25*L
sy = 1.00*0.25*L

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.move_to( -L*2.5/100, 1.13*sx )
draw.line_to( -L*2.5/100, 0.87*sy )
draw.line_to( -2.5*L*2.5/100, 0.8*sy )
draw.line_to( -2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.move_to( L*2.5/100, 1.13*sx )
draw.line_to( L*2.5/100, 0.87*sy )
draw.line_to( 2.5*L*2.5/100, 0.8*sy )
draw.line_to( 2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()

draw.save()

draw.rectangle( -2.5*L*2.5/100, -0.1*sy, 2.5*L*5/100, 0.9*sy )
draw.clip()

draw.translate( 0, 0 )
draw.scale( 2.5, 2.5 )

sx = 0.0 * 0.25 * L
sy = 0.3 * 0.25 * L

# show matchings aways from boundary
for match in matchings :
    x = match[0]
    y = match[1]

    set_color( draw, Tango['Aluminium5'] )
    draw.move_to( X[x], sx )
    draw.line_to( Y[y], sy )
    draw.stroke()
    
    set_color( draw, Tango['SkyBlue3'] )
    draw.arc( X[x], sx, L/500, 0, 2*pi )
    draw.fill()
    
    set_color( draw, Tango['ScarletRed3'] )
    draw.arc( Y[y], sy, L/500, 0, 2*pi )
    draw.fill()

draw.restore()

#
# draw zoom area to the left
#

sx = 0.95*0.25*L
sy = 1.00*0.25*L

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.translate( -L*42.5/100, 0 )
draw.move_to( -L*2.5/100, 1.13*sx )
draw.line_to( -L*2.5/100, 0.87*sy )
draw.line_to( -2.5*L*2.5/100, 0.8*sy )
draw.line_to( -2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()

draw.save()
draw.set_line_width( L / 500 )
set_color( draw, Tango['Orange2'] )
draw.translate( -L*42.5/100, 0 )
draw.move_to( L*2.5/100, 1.13*sx )
draw.line_to( L*2.5/100, 0.87*sy )
draw.line_to( 2.5*L*2.5/100, 0.8*sy )
draw.line_to( 2.5*L*2.5/100, -0.05*sy )
draw.stroke()
draw.restore()
    
draw.save()

draw.rectangle( -L*42.5/100 - 2.5*L*2.5/100, -0.1*sy, 2.5*L*5/100, 0.9*sy )
draw.clip()

# draw.save()
# draw.set_line_width( L/1000 )
# set_color( draw, Tango['Plum3'] )
# draw.move_to( -L*42.5/100, 0 )
# draw.line_to( -L*42.5/100, L/3 )
# draw.stroke()
# draw.restore()

draw.scale( 2.5, 2.5 )
draw.translate( L*25.5/100, 0 )

# draw.save()
# draw.set_line_width( L/1000 )
# set_color( draw, Tango['Chameleon3'] )
# draw.move_to( -L*42.5/100, 0 )
# draw.line_to( -L*42.5/100, L/3 )
# draw.stroke()
# draw.restore()

sx = 0.0 * 0.25 * L
sy = 0.3 * 0.25 * L

# show matchings aways from boundary
for match in matchings :
    x = match[0]
    y = match[1]

    set_color( draw, Tango['Aluminium5'] )
    draw.move_to( X[x], sx )
    draw.line_to( Y[y], sy )
    draw.stroke()

    set_color( draw, Tango['SkyBlue3'] )
    draw.arc( X[x], sx, L/500, 0, 2*pi )
    draw.fill()
    
    set_color( draw, Tango['ScarletRed3'] )
    draw.arc( Y[y], sy, L/500, 0, 2*pi )
    draw.fill()

draw.restore()

    
# surface.write_to_png( "a3.png" )
draw.show_page()
surface.finish()
