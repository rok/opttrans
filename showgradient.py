#!/usr/bin/env python3

import pdb

import sys
import numpy as np
import pickle

from math import sqrt

filename = 'data.dat'

if len( sys.argv ) > 1 :
    filename = sys.argv[1]
    
datafile = open( filename, 'rb' )

L = pickle.load( datafile )
n = pickle.load( datafile )
X = pickle.load( datafile )
Y = pickle.load( datafile )

matchings = pickle.load( datafile )
crossing  = pickle.load( datafile )
cxpoints  = pickle.load( datafile )
normval   = pickle.load( datafile )
normpos   = pickle.load( datafile )

from gradient             import *
from matchings_avg        import *
from gradient_avg_coarse  import *
from matchings_avg_coarse import *
from grid                 import *

lb = 1*L/4
ub = 3*L/4

ncells = len( gradient )

#
# number of matchings within inner domain
#

nmatch = 0

for x in X :
    if ( x[0] >= lb and x[0] <= ub and x[1] >= lb and x[1] <= ub ) :
        nmatch += 1

for y in Y :
    if ( y[0] >= lb and y[0] <= ub and y[1] >= lb and y[1] <= ub ) :
        nmatch += 1

#
# determine average length of matchings
#

avg_d = 0

for pair in matchings :
    dx = Y[pair[1]][0] - X[pair[0]][0]
    dy = Y[pair[1]][1] - X[pair[0]][1]
    d  = sqrt( dx*dx + dy*dy )
    avg_d += d

avg_d = avg_d / len(matchings)

print( "average matching length: %.4e" % avg_d )

#
# compute error (assuming same order of coordinates in gradient/matchings)
#

error = 0.0

for i in range( len(gradient) ) :
    grad   = [ gradient[i][2], gradient[i][3] ]
    match  = [ matchings_avg[i][2], matchings_avg[i][3] ]
    diff   = [ match[0] - grad[0], match[1] - grad[1] ]
    error += ( diff[0] * diff[0] + diff[1] * diff[1] )

print( "error = %.4e / %.4e / %.4e" % ( error, error / ncells, error / ( avg_d * ncells ) ) )

#
# decompose into X/Y/U/V components
#

gX = []
gY = []
gU = []
gV = []

for data in gradient :
    gX.append( data[0] )
    gY.append( data[1] )
    gU.append( data[2] )
    gV.append( data[3] )

mX = []
mY = []
mU = []
mV = []

for data in matchings_avg :
    mX.append( data[0] )
    mY.append( data[1] )
    mU.append( data[2] )
    mV.append( data[3] )

ngrad = len( gradient )

#
# show gradient
#

from common import *

size   = [ 1000, 1000 ]

# surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
surface = cairo.PDFSurface( "gradient.pdf", size[0], size[1] )
draw    = cairo.Context( surface )

set_color( draw, Tango['White'] )
draw.rectangle( 0, 0, size[0], size[1] )
draw.fill()

draw.set_line_width( 0.1 )
draw.translate( size[0] * 0.025, size[1]*0.025 )
draw.scale( size[0] / (1.05*L), size[0] / (1.05*L) )
draw.scale( L / ( ub-lb ), L / (ub-lb) )
draw.translate( -lb, -lb )

# show gradient
set_color( draw, Tango['ScarletRed3'] )

for data in gradient :
    draw.move_to( data[0], data[1] )
    draw.line_to( data[0] + data[2], data[1] + data[3] )
    draw.stroke()

# show matchings
set_color( draw, Tango['SkyBlue3'] )

for data in matchings_avg :
    draw.move_to( data[0], data[1] )
    draw.line_to( data[0] + data[2], data[1] + data[3] )
    draw.stroke()

# draw grid
set_color( draw, Tango['Aluminium4'] )
draw.rectangle( lb, lb, ub-lb, ub-lb )
draw.stroke()

pos = lb + cell_len
while pos < ub :
    draw.move_to( pos, lb )
    draw.line_to( pos, ub )
    draw.stroke()

    draw.move_to( lb, pos )
    draw.line_to( ub, pos )
    draw.stroke()

    pos += cell_len

# surface.write_to_png( "gradient.png" )
surface.finish()

#
# show coarse gradient
#

# surface = cairo.ImageSurface( cairo.FORMAT_ARGB32, size[0], size[1] )
surface = cairo.PDFSurface( "gradient_coarse.pdf", size[0], size[1] )
draw    = cairo.Context( surface )

draw.save()

set_color( draw, Tango['White'] )
draw.rectangle( 0, 0, size[0], size[1] )
draw.fill()

draw.set_line_width( 0.1 )
draw.translate( size[0] * 0.05, size[1]*0.05 )
draw.scale( size[0] / (1.1*L), size[0] / (1.1*L) )
draw.scale( L / ( ub-lb ), L / (ub-lb) )
draw.translate( -lb, -lb )

# show gradient
draw.save()

set_color( draw, Tango['ScarletRed3'] )

for data in gradient_avg_coarse :
    draw.move_to( data[0], data[1] )
    draw.line_to( data[0] + data[2], data[1] + data[3] )
    draw.stroke()

draw.restore()

# show matchings
draw.save()

set_color( draw, Tango['SkyBlue3'] )

for data in matchings_avg_coarse :
    draw.move_to( data[0], data[1] )
    draw.line_to( data[0] + data[2], data[1] + data[3] )
    draw.stroke()

draw.restore()

# draw grid
draw.save()

set_color( draw, Tango['Aluminium4'] )
draw.rectangle( lb, lb, ub-lb, ub-lb )
draw.stroke()

pos = lb + coarse_cell_len
while pos < ub :
    draw.move_to( pos, lb )
    draw.line_to( pos, ub )
    draw.stroke()

    draw.move_to( lb, pos )
    draw.line_to( ub, pos )
    draw.stroke()

    pos += coarse_cell_len

draw.restore()

draw.save()

yofs = (ub-lb)*0.006
text = "R = %.1f" % float(coarse_cell_len)
draw.set_font_size( 3 )
draw.select_font_face( "Roboto", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL )
xbearing, ybearing, width, height, dx, dy = draw.text_extents( text )
set_color( draw, Tango['SkyBlue3'] )

draw.move_to( ( ub + lb ) / 2.0 - 0.5*width, ub + yofs + height )
draw.show_text( text )

# draw.move_to( lb, lb )
# draw.rotate( -pi/2.0 )
# draw.move_to( -lb - 0.5*coarse_cell_len - 0.5*width, lb - yofs )
# draw.show_text( text )

draw.restore()

draw.restore()

# surface.write_to_png( "gradient_coarse.png" )
surface.finish()
    
