#!/usr/bin/env python3
#
# ffmpeg -r 0.5 -f image2 -pattern_type glob -i 'a1_*.png' -vcodec libx264 -crf 10 -pix_fmt yuv420p a1.mp4
#

import numpy as np
import ot
import random

from time import time
from math import sqrt

#
# import matplotlib
#

import matplotlib as mpl

theme = { 'text.usetex'           : False,
          'font.family'           : 'Roboto Condensed',
          # 'figure.facecolor'      : "#fdfdf6",
          'axes.titlesize'        : 16,
          'axes.titlelocation'    : 'center',
          'axes.labelsize'        : 18,
          'axes.labelcolor'       : "#000000",
          'lines.markeredgewidth' : 0.0,
          'lines.markersize'      : 0,
          'lines.markeredgecolor' : 'none',
          'lines.linewidth'       : 3,
          'legend.fontsize'       : 16,
          'legend.framealpha'     : 1.0,
          'xtick.labelsize'       : 14,
          'ytick.labelsize'       : 18,
          'figure.figsize'        : ( 10, 10 )
         }

colors = [ '#888a85',
           '#3465a4',
           '#cc0000',
           '#4e9a06',
           '#f57900',
           '#75507b',
           '#c17d11' ]

line_styles = [ 'solid', 'dashed', 'dotted', 'dashdot' ]

mpl.use('Agg')
mpl.rcParams.update( theme  )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages
from   mpl_toolkits.mplot3d            import Axes3D

nfig = 0

#############################################################

#
# compute weight of matching
#
def matching_weight ( matching, X, Y ) :
    weight = 0
    for [ x, y ] in matching :
        weight += np.linalg.norm( X[x] - Y[y] )

    return weight

#
# show matching
#
def show_matching ( matching, X, Y, L, is_opt = False ) :

    global nfig
    
    z0_pos = 0
    z1_pos = 1
    
    fig = plt.figure( nfig, figsize = (10, 10) )
    nfig += 1
    
    # Generating a 3D sine wave
    ax = plt.axes( projection = '3d' )
    
    ax.view_init( 20, -15 )
    
    # decompose into components
    Xx = [ x[0] for x in X ]
    Xy = [ x[1] for x in X ]
    Xz = [ z0_pos for x in X ]
    
    Yx = [ y[0] for y in Y ]
    Yy = [ y[1] for y in Y ]
    Yz = [ z1_pos for y in Y ]
    
    # plot LxL square
    ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ z0_pos, z0_pos, z0_pos, z0_pos, z0_pos ], color = '#000000' )
    ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ z1_pos, z1_pos, z1_pos, z1_pos, z1_pos ], color = '#000000' )

    # show matching
    for match in matching :
        x = match[0]
        y = match[1]
        ax.plot( [ X[x][0], Y[y][0] ], [ X[x][1], Y[y][1] ], [ z0_pos, z1_pos ], color = colors[0], linewidth = 2.0 )
        
    # show coordinates
    ax.scatter( Xx, Xy, Xz, s = 200, color = colors[1], alpha = 1.0 )
    ax.scatter( Yx, Yy, Yz, s = 200, color = colors[2], alpha = 1.0 )

    # plt.annotate( "Iteration : #%d" % ( maxiter-1 ), [ -0.03, -0.07 ], fontsize = 32 )
    weight = matching_weight( matching, X, Y )
    if is_opt :
        plt.annotate( "cost    : %.3f  (optimal)" % ( sqrt(weight) ), [ -0.03, -0.08 ], fontsize = 32, color = 'r' )
    else:
        plt.annotate( "cost    : %.3f" % ( sqrt(weight) ), [ -0.03, -0.08 ], fontsize = 32 )
    
    # trun off/on axis
    plt.axis( 'off' )
    plt.tight_layout()

    plt.savefig( "a1_%04d.png" % (nfig-1) )
    plt.close()

#############################################################

#
# problem size
#

seed = random.randint( 0, 2147483647 )
seed = 1971199453
# seed = 1910413941
print( "seed = %d" % seed )
np.random.seed( seed )

L = 1.0
n = 7

print( "setting up 2 × %d samples" % n )

#
# set up random coordinates
#

X = np.zeros( [ n, 2 ] )
Y = np.zeros( [ n, 2 ] )

for i in range( n ) :
    X[i] = [ np.random.uniform( 0, L ), np.random.uniform( 0, L ) ]
    Y[i] = [ np.random.uniform( 0, L ), np.random.uniform( 0, L ) ]

#
# define initial matching
#
matching = []
weight   = 0

for i in range( n ) :
    matching.append( [ i, i ] )

# compute optimal matching
opt_matching = matching
opt_weight   = matching_weight( matching, X, Y )

for i0 in range(n) :
    for i1 in range(n) :
        if i1 == i0 : continue
        for i2 in range(n) :
            if i2 in [ i0, i1 ] : continue
            for i3 in range(n) :
                if i3 in [ i0, i1, i2 ] : continue
                for i4 in range(n) :
                    if i4 in [ i0, i1, i2, i3 ] : continue
                    for i5 in range(n) :
                        if i5 in [ i0, i1, i2, i3, i4 ] : continue
                        for i6 in range(n) :
                            if i6 in [ i0, i1, i2, i3, i4, i5 ] : continue
                            tmatch = [ [ 0, i0 ],
                                       [ 1, i1 ],
                                       [ 2, i2 ],
                                       [ 3, i3 ],
                                       [ 4, i4 ],
                                       [ 5, i5 ],
                                       [ 6, i6 ] ]
                            tweight = matching_weight( tmatch, X, Y )
                            
                            if tweight < opt_weight :
                                opt_matching = tmatch
                                opt_weight   = tweight

print( matching )
print( opt_matching )

matching = [[0, 0], [1, 1], [2, 2], [3, 3], [4, 4], [5, 5], [6, 6]]
show_matching( matching, X, Y, L )

matching = [[0, 4], [1, 1], [2, 2], [3, 3], [4, 0], [5, 5], [6, 6]]
show_matching( matching, X, Y, L )

matching = [[0, 4], [1, 6], [2, 2], [3, 3], [4, 0], [5, 5], [6, 1]]
show_matching( matching, X, Y, L )

matching = [[0, 4], [1, 6], [2, 5], [3, 3], [4, 0], [5, 2], [6, 1]]
show_matching( matching, X, Y, L )

matching = [[0, 4], [1, 6], [2, 5], [3, 0], [4, 3], [5, 2], [6, 1]]
show_matching( matching, X, Y, L, True )

matching = [[0, 6], [1, 5], [2, 4], [3, 0], [4, 3], [5, 2], [6, 1]]
show_matching( matching, X, Y, L )

# show_matching( opt_matching, X, Y, L )

# for niter in range( 10 ) :
    
# for maxiter in range( 1, 600, 5 ) :
#     # compute EMD
#     tic = time()
#     G0 = ot.emd( a, b, M, maxiter, numThreads = 8 )
#     toc = time()

#     matching = []
#     weight    = 0

#     for x in range( n ) :
#         y = np.argmax( G0[x] )
#         matching.append( [ x, y ] )
#         weight += np.linalg.norm( X[x] - Y[y] )

#     print( "found matching in %.2fs, weight = %.4e" % ( ( toc - tic ), weight ) )

#     show_matching( matching, X, Y, L )
