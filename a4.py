#!/usr/bin/env python3

import sys
import pickle

#
# import data
#

filename = 'data.dat'

if len( sys.argv ) > 1 :
    filename = sys.argv[1]
    
datafile = open( filename, 'rb' )

L = pickle.load( datafile )
n = pickle.load( datafile )
X = pickle.load( datafile )
Y = pickle.load( datafile )

matchings = pickle.load( datafile )
crossing  = pickle.load( datafile )
cxpoints  = pickle.load( datafile )
normval   = pickle.load( datafile )
normpos   = pickle.load( datafile )

lb = 1*L/4
ub = 3*L/4

#
# import matplotlib
#

import matplotlib as mpl

theme = { 'text.usetex'           : False,
          'font.family'           : 'Roboto Condensed',
          # 'figure.facecolor'      : "#fdfdf6",
          'axes.titlesize'        : 16,
          'axes.titlelocation'    : 'center',
          'axes.labelsize'        : 18,
          'axes.labelcolor'       : "#000000",
          'lines.markeredgewidth' : 0.0,
          'lines.markersize'      : 0,
          'lines.markeredgecolor' : 'none',
          'lines.linewidth'       : 3,
          'legend.fontsize'       : 16,
          'legend.framealpha'     : 1.0,
          'xtick.labelsize'       : 14,
          'ytick.labelsize'       : 18,
          'figure.figsize'        : ( 10, 10 )
         }

colors = [ '#888a85',
           '#3465a4',
           '#cc0000',
           '#4e9a06',
           '#f57900',
           '#75507b',
           '#c17d11' ]

line_styles = [ 'solid', 'dashed', 'dotted', 'dashdot' ]

mpl.use('Agg')
mpl.rcParams.update( theme  )

import matplotlib.pyplot               as     plt
from   matplotlib.backends.backend_pdf import PdfPages
from   mpl_toolkits.mplot3d            import Axes3D

nfig = 1

#############################################################

#
# show samples
#

# Change the Size of Graph using
# Figsize
fig = plt.figure( nfig, figsize = (20, 20) )
nfig += 1
 
# Generating a 3D sine wave
ax = plt.axes( projection = '3d' )

ax.view_init( 20, -20 )

# decompose into components
Xx = [ x[0] for x in X ]
Xy = [ x[1] for x in X ]
Xz = [ 0    for x in X ]
 
Yx = [ y[0] for y in Y ]
Yy = [ y[1] for y in Y ]
Yz = [ 1    for y in Y ]

# show matchings
for match in matchings :
    x = match[0]
    y = match[1]
    ax.plot( [ X[x][0], Y[y][0] ], [ X[x][1], Y[y][1] ], [ 0, 1 ], color = colors[0], linewidth = 0.5 )

# show coordinates
ax.scatter( Xx, Xy, Xz, color = colors[1], alpha = 1.0 )
ax.scatter( Yx, Yy, Yz, color = colors[2], alpha = 1.0 )

# plot LxL square
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ 0, 0, 0, 0, 0 ], color = '#000000' )
ax.plot( [ 0, L, L, 0, 0 ], [ 0, 0, L, L, 0 ], [ 1, 1, 1, 1, 1 ], color = '#000000' )

# trun off/on axis
plt.axis( 'off' )
plt.tight_layout()

plt.savefig( "a4.png" )

