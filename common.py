import numpy as np
import ot
import random

from time import time
from math import sqrt, pi, sin, cos

import cairo

Tango = { 'Butter1' : [ 252,233,79 ],
          'Butter2' : [ 237,212,0 ],
          'Butter3' : [ 196,160,0 ],

          'Chameleon1' : [ 138,226,52 ],
          'Chameleon2' : [ 115,210,22 ],
          'Chameleon3' : [ 78,154,6 ],

          'Orange1' : [ 252,175,62 ],
          'Orange2' : [ 245,121,0 ],
          'Orange3' : [ 206,92,0 ],

          'SkyBlue1' : [ 114,159,207 ],
          'SkyBlue2' : [ 52,101,164 ],
          'SkyBlue3' : [ 32,74,135 ],

          'Plum1' : [ 173,127,168 ],
          'Plum2' : [ 117,80,123 ],
          'Plum3' : [ 92,53,102 ],

          'Chocolate1' : [ 233,185,110 ],
          'Chocolate2' : [ 193,125,17 ],
          'Chocolate3' : [ 143,89,2 ],

          'ScarletRed1' : [ 239,41,41 ],
          'ScarletRed2' : [ 204,0,0 ],
          'ScarletRed3' : [ 164,0,0 ],

          'Aluminium1' : [ 238,238,236 ],
          'Aluminium2' : [ 211,215,207 ],
          'Aluminium3' : [ 186,189,182 ],
          'Aluminium4' : [ 136,138,133 ],
          'Aluminium5' : [ 85,87,83 ],
          'Aluminium6' : [ 46,52,54 ],

          'White' : [ 255, 255, 255 ],
          'Black' : [ 0, 0, 0 ],
         }

def set_color ( draw, color ) :
    draw.set_source_rgb( color[0] / 255.0,
                         color[1] / 255.0,
                         color[2] / 255.0 )

def set_color_alpha ( draw, color, alpha ) :
    draw.set_source_rgba( color[0] / 255.0,
                          color[1] / 255.0,
                          color[2] / 255.0,
                          alpha )
    
